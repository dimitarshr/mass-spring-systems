#pragma once

#include "Mesh.h"
#include "Force.h"

class Body {

public:
	// Defaulf constructor.
	Body();
	~Body();

	/*
	** GET METHODS
	*/
	Mesh &getMesh() { return m_mesh; }

	// Transform matrices
	glm::mat3 getTranslate() const { return m_mesh.getTranslate(); }
	glm::mat3 getRotate() const { return m_mesh.getRotate(); }
	glm::mat3 getScale() const { return m_mesh.getScale(); }

	// Dynamic variables
	glm::vec3& getAcc() { return m_acc; }
	glm::vec3& getVel() { return m_vel; }
	glm::vec3& getPos() { return m_pos; }

	// Physical properties
	float getMass() const { return m_mass; }
	float getCor() { return m_cor; }

	/*
	** SET METHODS
	*/
	// Mesh
	void setMesh(Mesh m) { m_mesh = m; }

	// Dynamic variables
	void setAcc(const glm::vec3 vect) { m_acc = vect; }
	void setVel(const glm::vec3 vect) { m_vel = vect; }
	void setVel(int i, float p) { m_vel[i] = p; }
	void setPos(const glm::vec3 vect) { m_pos = vect; m_mesh.setPos(vect); }
	void setPos(int i, float p) { m_pos[i] = p; m_mesh.setPos(i, p); }

	// Physical properties
	void setCor(float cor) { m_cor = cor; }
	void setMass(float mass) { m_mass = mass; }

	/*
	** OTHER METHODS
	*/

	// Tranformation methods
	void translate(const glm::vec3 &vect);
	void rotate(float angle, const glm::vec3 &vect);
	void scale(const glm::vec3 &vect);

	glm::vec3 applyForces(glm::vec3 x, glm::vec3 v, float t, float dt);

	void addForce(Force *f)
	{
		m_forces.push_back(f);

		if (dynamic_cast<AerodynamicForce*>(f) != nullptr)
		{
			this->aeroForce.push_back(dynamic_cast<AerodynamicForce*>(f));
		}
	}

	std::vector<AerodynamicForce*> getAeroForce() {
		return this->aeroForce;
	}

private:
	Mesh m_mesh;

	// Mass
	float m_mass;
	// Coefficient of restitution
	float m_cor;

	// Acceleration
	glm::vec3 m_acc;
	// Velocity
	glm::vec3 m_vel;
	// Position
	glm::vec3 m_pos;

	std::vector<AerodynamicForce*>aeroForce;

	// Forces
	std::vector<Force*> m_forces;
};