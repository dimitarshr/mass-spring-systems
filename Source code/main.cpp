#pragma once
// Math constants
#define _USE_MATH_DEFINES
#include <cmath>  
#include <random>

// Std. Includes
#include <string>
#include <time.h>

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/matrix_operation.hpp>
#include "glm/ext.hpp"

// Other Libs
#include "SOIL2/SOIL2.h"

// project includes
#include "Application.h"
#include "Shader.h"
#include "Mesh.h"
#include "Particle.h"


// time
GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

void collisionDetection(Particle *p, Mesh cube) {
	for (int index = 0; index <= 2; index++) {
		if (p->getPos()[index] < (cube.getPos()[index] - 0.5f * cube.getScale()[index][index])) {
			p->setVel(index, p->getVel()[index] * -1.0f);
			p->setVel(p->getVel() * 0.95f);
			p->setPos(index, (cube.getPos()[index] - 0.5f * cube.getScale()[index][index]));
		}
		else if (p->getPos()[index] > (cube.getPos()[index] + 0.5f * cube.getScale()[index][index])) {
			p->setVel(index, p->getVel()[index] * -1.0f);
			p->setVel(p->getVel() * 0.95f);
			p->setPos(index, (cube.getPos()[index] + 0.5f * cube.getScale()[index][index]));
		}
	}
}

void addHookeForcesToChain(Gravity *gravityForce, Drag *dragForce, std::vector<Particle*> chain, float springStiffnessValue, float damplingCoeffValue, float restValue) {
	for (int i = 0; i <= chain.size() - 1; i++) {
		Particle *particle = chain[i];

		particle->addForce(gravityForce);
		particle->addForce(dragForce);

		if (i == 0) {
			Hooke *hooke = new Hooke(particle, chain[i + 1], springStiffnessValue, damplingCoeffValue, restValue);
			particle->addForce(hooke);
		}
		else if (i == chain.size() - 1) {
			Hooke *hooke = new Hooke(particle, chain[i - 1], springStiffnessValue, damplingCoeffValue, restValue);
			particle->addForce(hooke);
		}
		else {
			Hooke *hooke = new Hooke(particle, chain[i - 1], springStiffnessValue, damplingCoeffValue, restValue);
			particle->addForce(hooke);

			Hooke *hooke2 = new Hooke(particle, chain[i + 1], springStiffnessValue, damplingCoeffValue, restValue);
			particle->addForce(hooke2);
		}
	}
}

void addHookeForcesToTrampoline(Gravity *gravityForce, Drag *dragForce, std::vector<std::vector<Particle*>> trampoline, float springStiffnessValue, float damplingCoeffValue, float restValue) {
	for (int column = 0; column < trampoline.size(); column++) {
		std::vector<Particle*> columnParticles = trampoline[column];
		for (int row = 0; row < columnParticles.size(); row++) {
			Particle* p = columnParticles[row];

			p->addForce(gravityForce);
			p->addForce(dragForce);

			if (column != 0 && column != trampoline.size() - 1 && row != 0 && row != trampoline.size() - 1) {
				Hooke *hookeTop = new Hooke(p, trampoline[column][row-1], springStiffnessValue, damplingCoeffValue, restValue);
				Hooke *hookeLeft = new Hooke(p, trampoline[column-1][row], springStiffnessValue, damplingCoeffValue, restValue);
				Hooke *hookeRight = new Hooke(p, trampoline[column+1][row], springStiffnessValue, damplingCoeffValue, restValue);
				Hooke *hookeBottom = new Hooke(p, trampoline[column][row+1], springStiffnessValue, damplingCoeffValue, restValue);
				p->addForce(hookeTop);
				p->addForce(hookeLeft);
				p->addForce(hookeRight);
				p->addForce(hookeBottom);
			}
		}
	}
}

void addForcesToCloth(Gravity *gravityForce, Drag *dragForce, std::vector<std::vector<Particle*>> cloth, float springStiffnessValue, float damplingCoeffValue, float restValue) {
	for (int column = 0; column < cloth.size(); column++) {
		std::vector<Particle*> columnParticles = cloth[column];
		for (int row = 0; row < columnParticles.size(); row++) {
			Particle* p = columnParticles[row];

			p->addForce(gravityForce);

			if (column != 0 && column != cloth.size() - 1 && row != 0 && row != cloth.size() - 1) {
				Hooke *hookeTop = new Hooke(p, cloth[column][row - 1], springStiffnessValue, damplingCoeffValue, restValue);
				Hooke *hookeLeft = new Hooke(p, cloth[column - 1][row], springStiffnessValue, damplingCoeffValue, restValue);
				Hooke *hookeRight = new Hooke(p, cloth[column + 1][row], springStiffnessValue, damplingCoeffValue, restValue);
				Hooke *hookeBottom = new Hooke(p, cloth[column][row + 1], springStiffnessValue, damplingCoeffValue, restValue);
				p->addForce(hookeTop);
				p->addForce(hookeLeft);
				p->addForce(hookeRight);
				p->addForce(hookeBottom);

				p->addForce(new AerodynamicForce(p, cloth[column][row - 1], cloth[column - 1][row - 1]));
				p->addForce(new AerodynamicForce(p, cloth[column - 1][row - 1], cloth[column - 1][row]));
				p->addForce(new AerodynamicForce(p, cloth[column - 1][row], cloth[column][row + 1]));
				p->addForce(new AerodynamicForce(p, cloth[column][row + 1], cloth[column + 1][row + 1]));
				p->addForce(new AerodynamicForce(p, cloth[column + 1][row + 1], cloth[column + 1][row]));
				p->addForce(new AerodynamicForce(p, cloth[column + 1][row], cloth[column][row - 1]));
			}
			else if (column == 0 && row != 0) {
				Hooke *hookeTop = new Hooke(p, cloth[column][row - 1], springStiffnessValue, damplingCoeffValue, restValue);
				Hooke *hookeRight = new Hooke(p, cloth[column + 1][row], springStiffnessValue, damplingCoeffValue, restValue);
				p->addForce(hookeTop);
				p->addForce(hookeRight);

				p->addForce(new AerodynamicForce(p, cloth[column][row - 1], cloth[column + 1][row]));

				if (row != cloth.size() - 1) {
					Hooke *hookeBottom = new Hooke(p, cloth[column][row + 1], springStiffnessValue, damplingCoeffValue, restValue);
					p->addForce(hookeBottom);

					p->addForce(new AerodynamicForce(p, cloth[column + 1][row], cloth[column + 1][row + 1]));
					p->addForce(new AerodynamicForce(p, cloth[column + 1][row + 1], cloth[column][row + 1]));
				}
			}
			else if (column == cloth.size() - 1 && row != 0) {
				Hooke *hookeTop = new Hooke(p, cloth[column][row - 1], springStiffnessValue, damplingCoeffValue, restValue);
				Hooke *hookeLeft = new Hooke(p, cloth[column - 1][row], springStiffnessValue, damplingCoeffValue, restValue);
				p->addForce(hookeTop);
				p->addForce(hookeLeft);

				p->addForce(new AerodynamicForce(p, cloth[column][row - 1], cloth[column - 1][row - 1]));
				p->addForce(new AerodynamicForce(p, cloth[column - 1][row - 1], cloth[column - 1][row]));

				if (row != cloth.size() - 1) {
					Hooke *hookeBottom = new Hooke(p, cloth[column][row + 1], springStiffnessValue, damplingCoeffValue, restValue);
					p->addForce(hookeBottom);

					p->addForce(new AerodynamicForce(p, cloth[column - 1][row], cloth[column][row + 1]));
				}
			}
			else if (row == cloth.size() - 1) {
				Hooke *hookeTop = new Hooke(p, cloth[column][row - 1], springStiffnessValue, damplingCoeffValue, restValue);
				Hooke *hookeLeft = new Hooke(p, cloth[column - 1][row], springStiffnessValue, damplingCoeffValue, restValue);
				Hooke *hookeRight = new Hooke(p, cloth[column + 1][row], springStiffnessValue, damplingCoeffValue, restValue);
				p->addForce(hookeTop);
				p->addForce(hookeLeft);
				p->addForce(hookeRight);

				p->addForce(new AerodynamicForce(p, cloth[column - 1][row], cloth[column - 1][row - 1]));
				p->addForce(new AerodynamicForce(p, cloth[column - 1][row - 1], cloth[column][row - 1]));
				p->addForce(new AerodynamicForce(p, cloth[column][row - 1], cloth[column + 1][row]));
			}
			/****************************************************/
		}
	}
}

// main function
int main()
{
	// create application
	Application app = Application::Application();
	app.initRender();
	Application::camera.setCameraPosition(glm::vec3(0.0f, 5.0f, 20.0f));
			
	// create ground plane
	Mesh plane = Mesh::Mesh(Mesh::QUAD);
	// scale it up x5
	plane.scale(glm::vec3(5.0f, 5.0f, 5.0f));
	plane.setShader(Shader("resources/shaders/physics.vert", "resources/shaders/physics.frag"));

	// Create the cube objects
	Mesh cube = Mesh::Mesh("resources/models/cube.obj");
	cube.translate(glm::vec3(0.0f, 4.9f, 0.0f));
	cube.scale(glm::vec3(10.0f, 10.0f, 10.0f));
	cube.setShader(Shader("resources/shaders/physics.vert", "resources/shaders/solid_transparent.frag"));

	// time
	const double dt = 0.001;
	double startTime = (GLfloat) glfwGetTime();
	double accumulator = 0.0;

	/*******************************************************************/

	/*
	** Task 1
	*/
	std::vector<Particle*> chain;

	Gravity *gravity = new Gravity(glm::vec3(0.0f, -9.8f, 0.0f));
	// The parameters are: air density; drag coefficient; cross section
	Drag *drag = new Drag(1.225f, 1.05f, 0.1f);

	float springStiffnessValue = 20.0f;
	float damplingCoeffValue = 1.0f;
	float restValue = 0.5f;
	int particlesNo = 5;

	for (int i = 0; i <= particlesNo-1; i++) {
		Particle *particle = new Particle();
		particle->getMesh().setShader(Shader("resources/shaders/solid.vert", "resources/shaders/solid_blue.frag"));
		particle->setPos(glm::vec3(i+1.0f, 10.0f, 0.0f));
		particle->setVel(glm::vec3(0.0f, 0.0f, 0.0f));
		chain.push_back(particle);
	}

	addHookeForcesToChain(gravity, drag, chain, springStiffnessValue, damplingCoeffValue, restValue);
	/*******************************************************************/
	
	/*
	** Task 2
	*/
	std::vector<Particle*> necklace;
	int necklaceLen = 10;

	for (int i = 0; i < necklaceLen; i++) {
		Particle *p = new Particle();
		p->getMesh().setShader(Shader("resources/shaders/solid.vert", "resources/shaders/solid_blue.frag"));
		p->setPos(glm::vec3(i, 10.0f, 0.0f));
		p->setVel(glm::vec3(0.0f, 0.0f, 0.0f));
		necklace.push_back(p);
	}
	addHookeForcesToChain(gravity, drag, necklace, springStiffnessValue, damplingCoeffValue, restValue);
	/*******************************************************************/
	
	/*
	** Task 3
	*/
	std::vector<std::vector<Particle*>> trampoline;
	int trampolineHeight = 10;
	int trampolineWidth = 10;

	for (int column = 0; column < trampolineHeight; column++) {
		std::vector<Particle*> columnParticles;
		for (int row = 0; row < trampolineWidth; row++) {
			Particle *p = new Particle();
			p->getMesh().setShader(Shader("resources/shaders/solid.vert", "resources/shaders/solid_blue.frag"));
			p->setPos(glm::vec3(column-4.5f, 3.0f, row-4.5f));
			p->setVel(glm::vec3(0.0f, 0.0f, 0.0f));
			columnParticles.push_back(p);
		}
		trampoline.push_back(columnParticles);
	}
	
	addHookeForcesToTrampoline(gravity, drag, trampoline, springStiffnessValue, 4.0f, restValue);
	/*******************************************************************/

	/*
	** Task 4
	*/
	std::vector<std::vector<Particle*>> cloth;
	int clothHeight = 10;
	int clothWidth = 10;
	float clothStiffness = 70.0f;
	float clothDamping = 25.0f;
	float clothRestLen = 0.5f;

	for (int column = 0; column < clothHeight; column++) {
		std::vector<Particle*> columnParticles;
		for (int row = clothWidth-1; row >= 0; row--) {
			Particle *p = new Particle();
			p->rotate((GLfloat)M_PI/2.0f, glm::vec3(0.0f, 0.0f, -1.0f));
			p->getMesh().setShader(Shader("resources/shaders/solid.vert", "resources/shaders/solid_blue.frag"));
			p->setPos(glm::vec3(0.0f, row+0.5f, column-4.5f));
			p->setVel(glm::vec3(0.0f, 0.0f, 0.0f));
			columnParticles.push_back(p);
		}
		cloth.push_back(columnParticles);
	}
	addForcesToCloth(gravity, drag, cloth, clothStiffness, clothDamping, clothRestLen);
	/*******************************************************************/
	bool task1 = true;
	bool task2 = false;
	bool task3 = false;
	bool task4 = false;

	// Game loop
	while (!glfwWindowShouldClose(app.getWindow()))
	{
		if (app.keys[GLFW_KEY_1]) {
			task1 = true;
			task2 = false;
			task3 = false;
			task4 = false;

			double startTime = (GLfloat)glfwGetTime();
			double accumulator = 0.0;
			int index = 1;

			for (Particle *p : chain) {
				p->setPos(glm::vec3(index, 10.0f, 0.0f));
				p->setVel(glm::vec3(0.0f, 0.0f, 0.0f));
				index++;
			}
		}
		if (app.keys[GLFW_KEY_2]) {
			task1 = false;
			task2 = true;
			task3 = false;
			task4 = false;

			double startTime = (GLfloat)glfwGetTime();
			double accumulator = 0.0;
			int index = 1;

			for (Particle *p : necklace) {
				p->setPos(glm::vec3(index-5.0f, 10.0f, 0.0f));
				p->setVel(glm::vec3(0.0f, 0.0f, 0.0f));
				index++;
			}
		}
		if (app.keys[GLFW_KEY_3]) {
			task1 = false;
			task2 = false;
			task3 = true;
			task4 = false;

			double startTime = (GLfloat)glfwGetTime();
			double accumulator = 0.0;

			for (int column = 0; column < trampolineHeight; column++) {
				for (int row = 0; row < trampolineWidth; row++) {
					trampoline[column][row]->setPos(glm::vec3(column - 4.5f, 3.0f, row - 4.5f));
					trampoline[column][row]->setVel(glm::vec3(0.0f, 0.0f, 0.0f));
				}
			}
		}
		if (app.keys[GLFW_KEY_4]) {
			task1 = false;
			task2 = false;
			task3 = false;
			task4 = true;

			double startTime = (GLfloat)glfwGetTime();
			double accumulator = 0.0;

			for (int column = 0; column < cloth.size(); column++) {
				std::vector<Particle*> columnParticles = cloth[column];
				for (int row = 0; row < columnParticles.size(); row++) {
					Particle* p = columnParticles[row];
					p->setPos(glm::vec3(0.0f, 9.0f - row + 0.5f, 9.0f - column - 4.5f));
					p->setVel(glm::vec3(0.0f, 0.0f, 0.0f));
					if (p->getAeroForce().size() != 0) {
						for (AerodynamicForce* a : p->getAeroForce()) {
							a->setWindSpeed(glm::vec3(5.5f, 0.0f, 0.0f));
						}
					}
				}
			}
		}

		if (app.keys[GLFW_KEY_F] && task4) {
			for (int column = 0; column < cloth.size(); column++) {
				std::vector<Particle*> columnParticles = cloth[column];
				for (int row = 0; row < columnParticles.size(); row++) {
					Particle* p = columnParticles[row];
					if (p->getAeroForce().size() != 0) {
						for (AerodynamicForce* a : p->getAeroForce()) {
							a->setWindSpeed(glm::vec3(0.0f, 0.0f, 0.0f));
						}
					}
				}
			}
		}

		if (app.keys[GLFW_KEY_O] && task4) {
			for (int column = 0; column < cloth.size(); column++) {
				std::vector<Particle*> columnParticles = cloth[column];
				for (int row = 0; row < columnParticles.size(); row++) {
					Particle* p = columnParticles[row];
					if (p->getAeroForce().size() != 0) {
						for (AerodynamicForce* a : p->getAeroForce()) {
							a->setWindSpeed(glm::vec3(5.5f, 0.0f, 0.0f));
						}
					}
				}
			}
		}

		/*
		**	SIMULATION
		*/
		GLfloat newTime = (GLfloat)glfwGetTime();
		GLfloat frameTime = newTime - startTime;
		startTime = newTime;
		accumulator += frameTime;

		if (task1) {
			while (accumulator >= dt) {
				app.doMovement(dt);

				for (int index = 1; index <= chain.size() - 1; index++) {
					Particle *currentParticle = chain[index];
					currentParticle->setAcc(currentParticle->applyForces(currentParticle->getPos(), currentParticle->getVel(), newTime, dt));
					currentParticle->setVel(currentParticle->getVel() + dt * currentParticle->getAcc());
					currentParticle->translate(dt * currentParticle->getVel());

					collisionDetection(currentParticle, cube);
				}
				accumulator -= dt;
			}
		}

		if (task2) {
			while (accumulator >= dt) {
				app.doMovement(dt);

				for (int index = 1; index <= necklace.size() - 2; index++) {
					Particle *currentParticle = necklace[index];
					currentParticle->setAcc(currentParticle->applyForces(currentParticle->getPos(), currentParticle->getVel(), newTime, dt));
					currentParticle->setVel(currentParticle->getVel() + dt * currentParticle->getAcc());
					currentParticle->translate(dt * currentParticle->getVel());

					collisionDetection(currentParticle, cube);
				}
				accumulator -= dt;
			}
		}
		if (task3) {
			while (accumulator >= dt) {
				app.doMovement(dt);

				for (int column = 1; column < trampoline.size() - 1; column++) {
					std::vector<Particle*> columnParticles = trampoline[column];
					for (int row = 1; row < columnParticles.size() - 1; row++) {
						Particle* p = columnParticles[row];

						p->setAcc(p->applyForces(p->getPos(), p->getVel(), newTime, dt));
						p->setVel(p->getVel() + dt * p->getAcc());
						p->translate(dt * p->getVel());

						collisionDetection(p, cube);
					}
				}
				accumulator -= dt;
			}
		}

		if (task4) {
			while (accumulator >= dt) {
				app.doMovement(dt);

				for (int column = 0; column < cloth.size(); column++) {
					std::vector<Particle*> columnParticles = cloth[column];
					for (int row = 1; row < columnParticles.size(); row++) {
						Particle* p = columnParticles[row];

						p->setAcc(p->applyForces(p->getPos(), p->getVel(), newTime, dt));
						p->setVel(p->getVel() + dt * p->getAcc());
						if (glm::length(p->getVel()) > 5.0f) {
							p->setVel(p->getVel() * 0.5f);
						}
						p->translate(dt * p->getVel());
					}
				}
				accumulator -= dt;
			}
		}

		/*
		**	RENDER 
		*/		
		// clear buffer
		app.clear();
		// draw groud plane
		app.draw(plane);

		if (task1) {
			for (int index = 0; index < chain.size(); index++) {
				app.draw(chain[index]->getMesh());
			}
		}
		
		if (task2) {
			for (int index = 0; index < necklace.size(); index++) {
				app.draw(necklace[index]->getMesh());
			}
		}

		if (task3) {
			for (std::vector<Particle*> column : trampoline) {
				for (Particle* p : column) {
					app.draw(p->getMesh());
				}
			}
		}

		if (task4) {
			for (std::vector<Particle*> column : cloth) {
				for (Particle* p : column) {
					app.draw(p->getMesh());
				}
			}
		}

		// Draw the cube objects
		app.draw(cube);

		app.display();
	}

	app.terminate();

	return EXIT_SUCCESS;
}

